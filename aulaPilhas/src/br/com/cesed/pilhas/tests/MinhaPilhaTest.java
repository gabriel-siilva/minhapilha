package br.com.cesed.pilhas.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.com.cesed.pilhas.MinhaPilha;
import br.com.cesed.pilhas.exception.ListaVaziaException;

class MinhaPilhaTest {

	private MinhaPilha mp;
	
	@BeforeEach
	void setUp() throws Exception {
		mp = new MinhaPilha();
	}

	@AfterEach
	void tearDown() throws Exception {
		mp = null;
	}

	@DisplayName("Teste Push B�sico")
	@Test
	void testPush() {
		
		assertEquals(0, mp.size());
	}
	
	@DisplayName("Teste Push Tamanho")
	@Test
	void testPushSize() {
		mp.push("Elemento 1");
		mp.push("Elemento 2");
		mp.push("Elemento 3");
		
		assertEquals(3, mp.size());
	}
	
	@DisplayName("Teste Push array maior")
	@Test
	void testPushArray() {
		mp.push("Elemento 1");
		mp.push("Elemento 2");
		mp.push("Elemento 3");
		mp.push("Elemento 4");
		
		assertEquals(4, mp.size());
	}
	
	@DisplayName("Teste Push elemento null")
	@Test
	void testPushNull() {
		mp.push(null);
		
	}
	
	@DisplayName("Teste Pop B�sico")
	@Test
	void testPop() throws ListaVaziaException {
		mp.push("elemento");
		mp.pop();
		
		assertEquals(0, mp.size());
	}
	
	@DisplayName("pop() retorno")
	@Test
	void testPopReturn() throws ListaVaziaException {
		mp.push("A");
		
		assertEquals("A", mp.pop());
	}
	
	@DisplayName("pop() com pilha vazia")
	@Test
	void testPopEmpty() {
		Assertions.assertThrows(ListaVaziaException.class, () -> {
			mp.pop();
		});
	}
	
	

}
