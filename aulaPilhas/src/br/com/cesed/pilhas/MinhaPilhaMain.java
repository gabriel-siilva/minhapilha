package br.com.cesed.pilhas;

import br.com.cesed.pilhas.exception.ListaVaziaException;

public class MinhaPilhaMain {
	public static void main(String[] args) throws ListaVaziaException {
		MinhaPilha mP = new MinhaPilha();
		mP.push("A");
		mP.push("B");
		mP.push("C");
		
		System.out.println(mP.pop());
	}
}
