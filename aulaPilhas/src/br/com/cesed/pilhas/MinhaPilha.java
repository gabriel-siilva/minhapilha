package br.com.cesed.pilhas;

import br.com.cesed.pilhas.exception.ListaVaziaException;

public class MinhaPilha {
	public static final int TAMANHO_INICIAL = 3;
	private Object[] minhaPilha = new Object[TAMANHO_INICIAL];
	private int inseridos;

	public void push(Object elemento) {
		if (inseridos == minhaPilha.length) {
			Object[] novaPilha = new Object[minhaPilha.length + TAMANHO_INICIAL];

			for (int i = 0; i < minhaPilha.length; i++) {
				novaPilha[i] = minhaPilha[i];
			}

			minhaPilha = novaPilha;
		}

		minhaPilha[inseridos] = elemento;
		inseridos++;
	};

	/**
	 * Retorna o elemento no topo da pilha e o remove
	 * 
	 * @return Object
	 * @throws ListaVaziaException
	 */
	public Object pop() throws ListaVaziaException {
		
		Object elementoTopo = top();

		minhaPilha[inseridos - 1] = null;
		inseridos--;

		return elementoTopo;
	};

	/**
	 * Retorna o elemento no topo da pilha
	 * 
	 * @return Object
	 */
	public Object top() {
		return minhaPilha[inseridos - 1];
	};

	/**
	 * Retorna se a pilha est� vazia ou n�o
	 * 
	 * @return boolean
	 */
	public boolean isEmpty() {
		boolean empty = true;

		if (inseridos > 0) {
			empty = false;
		}

		return empty;
	};

	/**
	 * Retorna a quantidade de elementos na pilha
	 * 
	 * @return int
	 */
	public int size() {
		return inseridos;
	};

	/**
	 * Imprime todos os elementos da pilha
	 */
	public void print() {
		for (int i = 0; i < inseridos; i++) {
			System.out.println(minhaPilha[i]);
		}
	};
}
